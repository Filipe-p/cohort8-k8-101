# Kubernettes 101 :taco:

Our AMAZING CATDOG store is doing super well! And Docker compose has help us a lot.

However, we are victims of our own sucess and the website is being hard to sclae / manage. If only we had a way to:

- scale
- check health
- make it multi availability zone
- and take care of all the networking for containers...

WELL WE DO:
K8 is orchestration of containers :D

This is a open source, tool owned by the linux foundation. It was donated by google, but containers where always a linux thing.

JFYI - there are competitors to docker and kubernetes. The technology and conpt is what is most important

### Main components

We need to know who it works. Basic setup includes:

- Master k8 x1
- Worker nodes x2


Inside the Worker nodes we have Pods.

Pods can belong to a NameSpace. 

Inside Pods we have Containers. 

----- You see the problem? If we have two worker nodes... we can loadbalance using a AWS loadbalancer.. But if we then have POD and then containers... How will we load balance in this situation?

**Services!**
They serve! They serve traffic to pods of containers.

##### Master k8

This is where you write your code. Like in Ansible.
Mostly it will be YAML. Define the right services and deployment files.

**This is the most important machine. Teams ofter have two of them just in case one dies.**

**In terms of security, teams sometimes build a bastion before going into this machine.**

Lastly you can install a front and some distributions bring a front end. Easier to see what happening. 

#### Worker nodes

Master will give comands via SSH so they need to be connect to master.
Netowrking also needs to be considered. 

For Multi AZ, put on worker node in each Availability Zone.

They have docker installed and can launch containers inside the pods.

#### Pods

Exist inside worker nodes and are a collection of containers.
Can be assigned to a namespace.
Mostly they house the containers.

### Installing K8

There are serval ways of getting a cluster going. we'll use Kubespray.

##### pre requisits:

- x1 master node (8GM of ram, 30gb of space)
- x2 worker nodes  (t2.micro with 20gb of space)
- Ansible Machine working

For a live deployment you might need to dig deeper into:
- how many namespaces and pod will you have?
- how many containers in each? and how much traffic?

More on harware requierments: https://docs.kublr.com/installation/hardware-recommendation/

There are serveral ways to get going, We'll follow the kubesspray. 

https://kubernetes.io/docs/setup/production-environment/tools/kubespray/



#### 1) launch machine

- ansible
- k8 cluster (3 machines)


#### 2) add the new 3 machines to ansible

- copy the public key from ansible machine into the other 3 machines
- be able to ssh
- sort out networking

#### 3) create inventory of machines

- copy ~/kubespray/inventory/sample/inventory.ini
- add machines as host in inventory of machines in ansible

#### 4) Unsure - but possibly set AWS_DEFAULT_PROFILE=Academy

- in .bash_profile
- source it.

#### 5) run playbook again inventory 

```
ansible-playbook -i ~/kubespray/inventory/sample/inventory_aws_cluster.ini --become-user=root ~/kubespray/cluster.yml -b -v  --private-key=~/.ssh/id_ecdsa
```

#### get readin in the control plane

```

$ sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf

```


### Our first deployment.yml


First let us create a namespace

```
kubectl get ns

kubectl create ns <name>

kubectl get ns

touch ngix-app.yaml 
```


ngix-app.yaml 

** start with **
```yaml

apiVersion: apps/v1
kind: Deployment



```

** first section is Metadata **

this is just data about the deployment your going to do.
Does not specify what the deployment actuall does.

```yaml
metadata:
  name:	filipe-ngnix
  lables:
    app: webapp


```


** Second Section Spec **

your going to specify how you want your desired state for the deployment of the containers. Much like the desired state of an autoscale group.

And then you'll specify what to launch into this desired state of kubernetes. 

```yaml
spec: 
  replicas: 2
  selector:
    matchLabels:
      app: webapp
  
```

** Second Section template inside spec **

this is where you select the image to run. 

```yaml
spec:
  (...)
  template:
    metadata:
      labels:
       	app: webapp
    spec:
      containers:
      -	name: filipenginx
       	image: nginx:1.14.2
       	ports:
```

Then you can apply it become true.

```
kubectl apply -f ngix-app.yaml -n web-ns 
```

To check you have:

```
kubectl describe 
```